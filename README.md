# RICO X

Web Technologies Team Project

Problema pe care incercam s-o rezolvam are la baza o nevoie importanta. Cea de a utiliza timpul in cel mai eficient mod. Iar atunci cand vrei sa cumperi un produs, incerci sa te uiti la cat mai multe situri si sa gasesti cel la pretul rentabil, dar si care sa te ajute in caz de probleme cu produsul.
Iar cand te uiti printre atatea situri, rabdarea ta e pe terminate si ajungi uneori sa faci o decizie care sa nu fie pe placul portofelului tau...

Proiectul nostru doreste sa ajute utilizatorii sa gaseasca cat mai repede si cat mai eficient produsele pe care vor vrea sa le achizitioneze, oferind acestuia posibilitatea de a compara preturile la produsul respectiv de la mai multe magazine care il au in stoc. Clientul va putea in acelasi timp sa vada istoricul preturilor pentru a vedea cand ar putea veni o perioada cu oferte sau care magazin a fost mereu de partea clientului. In acelasi timp, le vom oferi o functionalitate de comunicare, in care ei vor putea schimba pareri si opinii despre produsul respectiv