<?php
class registerModel extends Model{
    function __construct(){
        parent::__construct();
    }
    public function run(){

        $statement = $this->db->prepare("SELECT ID FROM USERS WHERE email = :email");
        $statement->execute(array(
            ':email' => $_POST['email']
        ));
        $count = $statement->rowCount();
        if($count>0){
            header('location: /register');
            exit;
        }

        $statement = $this->db->prepare("SELECT ID FROM USERS WHERE Nickname = :nickname");
        $statement->execute(array(
            ':nickname' => $_POST['nickname']
        ));
        $count = $statement->rowCount();
        if($count>0){
            header('location: /register');
            exit;
        }

        $statement = $this->db->prepare(
            "INSERT INTO users (ID, Nume, Prenume, EMAIL, PASSWORD, Nickname) 
                    VALUES (NULL, :lastName, :firstName, :email, :password, :nickname)");
        $statement->execute(array(
            ':lastName' => $_POST['lastName'],
            ':firstName' => $_POST['firstName'],
            ':email' => $_POST['email'],
            ':password' => $_POST['password'],
            ':nickname' => $_POST['nickname']
        ));
        header('location: /login');
    }
}