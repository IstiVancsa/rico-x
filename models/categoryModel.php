<?php
class categoryModel extends Model
{
    function __construct()
    {
        parent::__construct();
    }

    public function getCategories(){
        $statement = $this->db->prepare("SELECT * FROM CATEGORIES");
        $statement->execute();
        $data = $statement->fetchAll();
        $count = $statement->rowCount();
        if($count>0){
            return $data;
        }
        return 0;
    }
}