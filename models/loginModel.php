<?php
class loginModel extends Model{
    function __construct(){
        parent::__construct();
    }
    public function run(){
        $statement = $this->db->prepare("SELECT ID FROM USERS WHERE email = :email and password = :password");
        $statement->execute(array(
            ':email' => $_POST['email'],
            ':password' => $_POST['password']
        ));
        $count = $statement->rowCount();
        if($count>0){
            Session::init();
            Session::set('loggedIn', true);
            header('location: /');
        }
        else{
            header('location: /login');
        }
    }
}