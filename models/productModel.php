<?php
class productModel extends Model{
    function __construct(){
        parent::__construct();
    }
    public function run($id){
        $statement = $this->db->prepare("SELECT * FROM PRODUCTS WHERE idProduct =:productId");
        $statement->execute(array(
            ':productId' => $id
        ));
        $data = $statement->fetchAll();
        $count = $statement->rowCount();
        if($count>0){
            return $data;
        }
        return 0;
    }

    public function get_products($page, $search, $sort, $category){


        if($search!=''){
            $statement = $this->db->prepare("SELECT * FROM PRODUCTS WHERE NUME LIKE '%".$search."%'");
        }else
            if($category != '') {//categoty selected
                if ($sort != '') {
                    switch ($sort) {
                        case "pretcresc":
                            $statement = $this->db->prepare("SELECT PRODUCTS.Nume, PRODUCTS.Pret, PRODUCTS.imagePath FROM PRODUCTS WHERE CategoryId =".$category);
                            //$statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY PRET LIMIT $offset, $rowsperpage");
                            break;
                        case "pretdesc":
                            $statement = $this->db->prepare("SELECT PRODUCTS.Nume, PRODUCTS.Pret, PRODUCTS.imagePath FROM PRODUCTS WHERE CategoryId =".$category);
                            //$statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY PRET DESC LIMIT $offset, $rowsperpage");
                            break;
                        case "numecresc":
                            $statement = $this->db->prepare("SELECT PRODUCTS.Nume, PRODUCTS.Pret, PRODUCTS.imagePath FROM PRODUCTS WHERE CategoryId =".$category);
                            //$statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY Nume LIMIT $offset, $rowsperpage");
                            break;
                        case "numedesc":
                            $statement = $this->db->prepare("SELECT PRODUCTS.Nume, PRODUCTS.Pret, PRODUCTS.imagePath FROM PRODUCTS WHERE CategoryId =".$category);
                            //$statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY Nume DESC LIMIT $offset, $rowsperpage");
                            break;
                    }
                } else {//no sort but category
                    $statement = $this->db->prepare("SELECT PRODUCTS.Nume, PRODUCTS.Pret, PRODUCTS.imagePath FROM PRODUCTS WHERE CategoryId =".$category);
                }
            }
            else{//no categoty selected
                if ($sort != '') {
                    switch ($sort) {
                        case "pretcresc":
                            $statement = $this->db->prepare("SELECT * FROM PRODUCTS");
                            break;
                        case "pretdesc":
                            $statement = $this->db->prepare("SELECT * FROM PRODUCTS");
                            break;
                        case "numecresc":
                            $statement = $this->db->prepare("SELECT * FROM PRODUCTS");
                            break;
                        case "numedesc":
                            $statement = $this->db->prepare("SELECT * FROM PRODUCTS");
                            break;
                    }
                }
                else{//no category, no sort
                    $statement = $this->db->prepare("SELECT * FROM PRODUCTS");
                }
            }
        $statement->execute();
        $count = $statement->rowCount();

        $rowsperpage = 6;
        $totalpages = ceil($count / $rowsperpage);

        // if current page is greater than total pages...
        if ($page > $totalpages) {
            // set current page to last page
            $page = $totalpages;
        } // end if
        // if current page is less than first page...
        if ($page < 1) {
            // set current page to first page
            $page = 1;
        } // end if

        // the offset of the list, based on current page
        $offset = ($page - 1) * $rowsperpage;
        if($search!=''){
            $statement = $this->db->prepare("SELECT * FROM PRODUCTS WHERE NUME LIKE '%".$search."%' LIMIT $offset, $rowsperpage");
        }else
            if($category != '') {//categoty selected
                if ($sort != '') {
                    switch ($sort) {
                        case "pretcresc":
                            $statement = $this->db->prepare("SELECT Nume, Pret, imagePath FROM PRODUCTS WHERE CategoryId =".$category." Order BY PRET LIMIT $offset, $rowsperpage");
                            //$statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY PRET LIMIT $offset, $rowsperpage");
                            break;
                        case "pretdesc":
                            $statement = $this->db->prepare("SELECT Nume, Pret, imagePath FROM PRODUCTS WHERE CategoryId =".$category." Order BY PRET DESC LIMIT $offset, $rowsperpage");
                            //$statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY PRET DESC LIMIT $offset, $rowsperpage");
                            break;
                        case "numecresc":
                            $statement = $this->db->prepare("SELECT Nume, Pret, imagePath FROM PRODUCTS WHERE CategoryId =".$category." Order BY Nume LIMIT $offset, $rowsperpage");
                            //$statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY Nume LIMIT $offset, $rowsperpage");
                            break;
                        case "numedesc":
                            $statement = $this->db->prepare("SELECT Nume, Pret, imagePath FROM PRODUCTS WHERE CategoryId =".$category." Order BY Nume DESC LIMIT $offset, $rowsperpage");
                            //$statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY Nume DESC LIMIT $offset, $rowsperpage");
                            break;
                    }
                } else {//no sort but category
                    $statement = $this->db->prepare("SELECT Nume, Pret, imagePath FROM PRODUCTS WHERE CategoryId =".$category." LIMIT $offset, $rowsperpage");
                }
            }
            else{//no categoty selected
                if ($sort != '') {
                    switch ($sort) {
                        case "pretcresc":
                            $statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY PRET LIMIT $offset, $rowsperpage");
                            break;
                        case "pretdesc":
                            $statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY PRET DESC LIMIT $offset, $rowsperpage");
                            break;
                        case "numecresc":
                            $statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY Nume LIMIT $offset, $rowsperpage");
                            break;
                        case "numedesc":
                            $statement = $this->db->prepare("SELECT * FROM PRODUCTS ORDER BY Nume DESC LIMIT $offset, $rowsperpage");
                            break;
                    }
                }
                else{//no category, no sort
                $statement = $this->db->prepare("SELECT * FROM PRODUCTS LIMIT $offset, $rowsperpage");
                }
            }
        $statement->execute();

        $data["products"] = $statement->fetchAll();
        $data["totalpages"] =$totalpages;

        if($count>0){
            return $data;
        }
        return 0;
    }
}