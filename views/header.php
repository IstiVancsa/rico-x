<!DOCTYPE html>
<html>
<head>
    <title><?php echo $page_title?></title>
    <link rel="shortcut icon" href="">
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="/public/css/default.css">
    <?php if($page_title == "Produs"){?>
    <link rel="stylesheet" type="text/css" href="/public/css/produs.css">
    <?php }?>

    <?php if($page_title == "Detalii"){?>
        <link rel="stylesheet" type="text/css" href="/public/css/detalii.css">
    <?php }?>

    <?php if($page_title == "Login"){?>
        <link rel="stylesheet" type="text/css" href="/public/css/Login.css">
    <?php }?>
</head>

<body>
    <nav class="navbar navbar-red fixed-top">
        <a href="http://rico-x.lh/" class="navbar-moto">AI LUA, DAR N-AI HABAR</a>
        <div class="navbar-menu">
            <ul class="navbar-ul mobile-hide">
                <li class="nav-item dropdown">
                    <a class="nav-link"  onclick="openCategorii()">Categorii</a>
                    <div class="dropdown-menu" id="categorii">
                        <?php foreach ($categories as $category){
                            if(isset($_GET['category'])){
                                $cat_url = str_replace("category=".$_GET['category'],"category=".$category['ID'],$url);
                            }else{
                                $cat_url = $url.'?category='.$category['ID'];
                            }

                            ?>
                        <a href="<?php echo $cat_url?>" class="dropdown-item"><?php echo $category["Nume"]?></a>
                        <?php }?>
                    </div>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link"  onclick="openFiltre()">Filtre</a>
                    <div class="dropdown-menu" id="filtre">
                        <?php

                        if(isset($_GET['sort'])){
                            $sort_url = str_replace("sort=".$_GET['sort'],"",$url);
                            $sort_url .= "&";
                        }else{
                            if($url == "http://rico-x.lh/"){
                                $sort_url = $url.'?';
                            }else{
                                $sort_url = $url.'&';
                            }

                        }
                        ?>
                        <?php echo "<a  href='{$sort_url}sort=pretcresc' class='dropdown-item'>Pret Crescator</a>"?>
                        <?php echo "<a  href='{$sort_url}sort=pretdesc' class='dropdown-item'>Pret Descrescator</a>"?>
                        <?php echo "<a  href='{$sort_url}sort=numecresc' class='dropdown-item'>Nume Crescator</a>"?>
                        <?php echo "<a  href='{$sort_url}sort=numedesc' class='dropdown-item'>Nume Descrescator</a>"?>
                    </div>
                </li>
            </ul>
            <form action="" class="search-form pr-2" type="GET">
                <input class="form-input search-input" type="text" id="search" name="search" placeholder="search" >
                <button class="button" type="submit">Search</button>
            </form>
            <a class="button" href="/login">LOGIN</a>
        </div>
    </nav>