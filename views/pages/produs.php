<?php require 'views/header.php';
$apidata = $apidata->data;
?>
    <section>
        <h1><?php echo $product[0]["Nume"];?></h1>
        <div  class="section">
            <img src="<?php echo $product[0]["imagePath"];?>" alt="Produs">

            <table>
                <tbody>
                <tr>
                    <td>Descriere</td>
                    <td><?php echo $product[0]["Descriere"];?></td>
                </tr>
                <tr>
                    <td style="font-style: normal">
                        <a style="text-decoration: none" href="#">Adauga comentariu</a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="detalii/<?php echo $product[0]["idProduct"];?>" style="font-style: normal;text-decoration: none">Specificatii</a>
                    </td>
                </tr>
                </tbody>
            </table>

        </div>
    </section>
    <table>
        <tbody>
        <?php foreach($apidata as $data) {?>
            <tr>
                <td>Pret</td>
                <td><?php   echo $data->Best_Price;?></td>
            </tr>
            <tr>
                <td>Link</td>
                <td><a href="<?php echo $data->Uri;?>" >Link</a></td>
            </tr>
        <?php }?>
        </tbody>
    </table>
    </section>

    <section>
        <h2 style="margin-left: 100px">Lista vanzatori</h2>
        <table class="table" id="t01">
            <tr>
                <th>Magazin</th>
                <th>Model</th>
                <th>Pret</th>
            </tr>
            <tr>
                <td>PcGarage</td>
                <td>Samsung</td>
                <td>1999 lei</td>
            </tr>
            <tr>
                <td>cel.ro</td>
                <td>Samsung</td>
                <td>1989 lei</td>
            </tr>
            <tr>
                <td>eMag</td>
                <td>Samsung</td>
                <td>1999 lei</td>
            </tr>
        </table>
    </section>

    <section>
        <h3 style="margin-left: 5%">Produse recomandate</h3>
        <table class="table2">
            <tr>
                <td><img src="/public/img/index.jpg" alt="Samsung"><br>
                    <a href="#">Phone1</a></td>
                <td><img src="/public/img/index.jpg" alt="Samsung"><br>
                    <a href="#">Phone2</a></td>
                <td><img src="/public/img/index.jpg" alt="Samsung"><br>
                    <a href="#">Phone3</a></td>
                <td><img src="/public/img/index.jpg" alt="Samsung"><br>
                    <a href="#">Phone4</a></td>
            </tr>
        </table>
    </section>

<?php require 'views/footer.php'; ?>