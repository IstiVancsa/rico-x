<?php require 'views/header.php'; ?>

<div class="container mt-4">
    <div class="row">
        <?php foreach($products as $product){ ?>
        <div class="col-4">
            <div class="produs mt-3">
                <img src="<?php echo $product["imagePath"];?>" alt="Samsung">
                <p><?php echo $product["Nume"];?><span class="ml-1"><?php echo $product["Pret"];?></span></p>
                <a href="http://rico-x.lh/produs/<?php echo $product["idProduct"];?>" class="button btn btn-success">detalii</a>
            </div>

        </div>
        <?php } ?>
    </div>
    <?php
    // range of num links to show
    $range = 3;
    if($url == "http://rico-x.lh/"){
        $page_url = $url.'?';
    }else{
        $page_url = $url.'&';
    }
    echo "<ul class='pagination'>";
    // if not on page 1, don't show back links
    if ($currentpage > 1) {
        // show << link to go back to page 1
        echo "<li class='page-item'> <a class='page-link' href='{$page_url}page=1'><<</a></li> ";
        // get previous page num
        $prevpage = $currentpage - 1;
        // show < link to go back to 1 page
        echo "<li class='page-item'>  <a class='page-link' href='{$page_url}page=$prevpage'><</a> </li>";
    } // end if

    // loop to show links to range of pages around current page
    for ($x = ($currentpage - $range); $x < (($currentpage + $range) + 1); $x++) {
        // if it's a valid page number...
        if (($x > 0) && ($x <= $totalpages)) {
            // if we're on current page...
            if ($x == $currentpage) {
                // 'highlight' it but don't make a link
                echo " <li class='page-item active'>  <a class='page-link' href='#'>$x</a> </li>";
                // if not current page...
            } else {
                // make it a link
                echo "<li class='page-item'>  <a class='page-link' href='{$page_url}page=$x'>$x</a>  </li>";
            } // end else
        } // end if
    } // end for

    // if not on last page, show forward and last page links
    if ($currentpage != $totalpages) {
        // get next page
        $nextpage = $currentpage + 1;
        // echo forward link for next page
        echo "<li class='page-item'>  <a class='page-link' href='{$page_url}page=$nextpage'>></a>  </li>";
        // echo forward link for lastpage
        echo "<li class='page-item'>  <a class='page-link' href='{$page_url}page=$totalpages'>>></a>  </li>";
    } // end if

    echo "</ul>"
    ?>

</div>

<?php require 'views/footer.php'; ?>