<html>
<link href='/public/css/login.css' rel='stylesheet' type='text/css'>

<form method="post" action="register/run">
    <div class="box">
        <h1>Register</h1>

        <input type="lastName" name="lastName" placeholder="lastName" onFocus="field_focus(this, 'lastName');" onblur="field_blur(this, 'lastName');" class="email" />

        <input type="firstName" name="firstName" placeholder="firstName" onFocus="field_focus(this, 'firstName');" onblur="field_blur(this, 'firstName');" class="email" />

        <input type="nickname" name="nickname" placeholder="nickname" onFocus="field_focus(this, 'nickname');" onblur="field_blur(this, 'nickname');" class="email" />

        <input type="email" name="email" placeholder="Email" onFocus="field_focus(this, 'email');" onblur="field_blur(this, 'email');" class="email" />

        <input type="password" name="password" placeholder="Password" onFocus="field_focus(this, 'password');" onblur="field_blur(this, 'password');" class="email" />

        <input type="submit" class="btn" value="SUBMIT"> <!-- End Btn -->

        <a href="login"><div id="btn2">Login</div></a> <!-- End Btn2 -->

    </div> <!-- End Box -->

</form>

<p>Forgot your password? <u style="color:#f1c40f;">Click Here!</u></p>

</html>