<?php require 'views/header.php';
?>
    <section>
        <h1><?php echo $product[0]["Nume"];?></h1>
        <div  class="section">
            <img src="<?php echo $product[0]["imagePath"];?>" alt="Produs">

            <table>
                <tbody>
                <tr>
                    <td>Descriere</td>
                    <td><?php echo $product[0]["Descriere"];?></td>
                </tr>

                </tbody>
            </table>

        </div>
    </section>
    <section>
        <div id="chartContainer" style="height: 370px; width: 50%; margin-left: 25%"></div>
        <script src="/public/js/canvasjs.js"></script>
    </section>

    <script src="/public/js/detalii.js"></script>

<?php require 'views/footer.php'; ?>