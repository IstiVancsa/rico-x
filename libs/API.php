<?php
class API {
    public function callApi($id){
        define ('URL', 'http://www.pricetree.com/dev/api.ashx?pricetreeId='.$id.'&apikey=7770AD31-382F-4D32-8C36-3743C0271699');

        $request = curl_init();
// Stabilim URL-ul serviciului
        curl_setopt ($request, CURLOPT_URL, URL);
// Rezultatul cererii va fi disponibil ca șir de caractere
        curl_setopt ($request, CURLOPT_RETURNTRANSFER, true);
// Nu verificam certificatul digital
        curl_setopt ($request, CURLOPT_SSL_VERIFYPEER, false);
// Executam cererea GET
        $response = curl_exec ($request);
// Închidem conexiunea
        curl_close ($request);
// Deserealizăm datele
        $data = json_decode($response);
// Afișăm conținutul variabilei $data.
        return $data;
    }
}

?>