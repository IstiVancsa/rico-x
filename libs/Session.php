<?php
/**
 * Created by PhpStorm.
 * User: Istvan
 * Date: 6/10/2019
 * Time: 12:29 AM
 */

class Session
{
    public static function init(){
        session_start();
    }

    public static function set($key, $value){
        $_SESSION[$key] = $value;
    }

    public static function get($key){
        return $_SESSION[$key];
    }

    public static function destroy(){
        session_destroy();
    }
}