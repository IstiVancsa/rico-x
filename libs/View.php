<?php
class View{
    function __construct(){
        //echo 'this is the view!<br />';
    }

    public function render($name,array $vars = array()){
        ob_start();
        extract($vars);

        require 'views/' . $name . '.php';
        return;
    }
}