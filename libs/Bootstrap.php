<?php
class Bootstrap{
    function __construct()
    {
        if(isset($_GET['url']))
            $url = $_GET['url'];
        else
            $url = null;
        $url = rtrim($url,'/');
        $url = explode('/', $url);

        //print_r($url);

        if(empty($url[0])){
            require 'controllers/home.php';
            $controller = new Home();
            return false;
        }

        $file = 'controllers/' . $url[0] . '.php';
        if(file_exists($file))
        {
            require $file;
        }
        else
        {
            require 'controllers/MyError.php';
            $error = new MyError();
            return false;
        }

        $controller = new $url[0];
        $controller->loadModel($url[0]);
        if(isset($url[2]))
        {
            $controller->{$url[1]}($url[2]);
        }else
            if(isset($url[1]))
            {
                    if(!is_numeric($url[1]))
                        $controller->{$url[1]}();
            }
    }
}