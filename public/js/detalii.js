window.onload = function () {

        var chart = new CanvasJS.Chart("chartContainer", {
            animationEnabled: true,
            title:{
                text: "Istoric Pret"
            },
            axisX:{
                interval:3,
                intervalType:"month"
            },
            axisY:{
                includeZero: false
            },
            data: [{
                type: "line",
                dataPoints: [
                    { x:new Date(2012,03,1),y: 1959},
                    { x:new Date(2012,04,3),y: 1969},
                    { x:new Date(2012,05,5),y: 1999, indexLabel: "Maxim",markerColor: "red", markerType:"triangle" },
                    { x:new Date(2012,06,7),y: 1899 },
                    { x:new Date(2012,07,9),y: 1959 },
                    { x:new Date(2012,08,11),y: 1969 },
        { x:new Date(2012,09,13),y: 1979 },
        { x:new Date(2012,10,15),y: 1949 },
        { x:new Date(2012,11,17),y: 1799 , indexLabel: "Minim",markerColor: "black", markerType: "cross" },
        { x:new Date(2012,12,19),y: 1929 },
        { x:new Date(2013,01,21),y: 1959 },
        { x:new Date(2013,02,23),y: 1969 }
    ]
    }]
    });
        chart.render();

    }