<?php

class MyError extends Controller {

    function __construct(){
        parent::__construct();
        echo 'This is an error!';
        $data['page_title'] = "Eroare";
        $this->view->msg = "This page doesn't exist!";
        $this->view->render('/error/index',$data);
    }

}