<?php
class Dashboard extends Controller {
    function __construct(){
        parent::__construct();
        $this->index();
        Session::init();
        $logged = Session::get('loggedIn');
        if($logged == false){
            Session::destroy();
            header('location: Login');
            exit;
        }
    }

    public function index(){
        $data['page_title'] = "Dashboard";
        $this->view->render('pages/dashboard',$data);
    }
}