<?php
class Register extends Controller {
    function __construct(){
        parent::__construct();
        $this->index();
    }

    public function index(){
        $data['page_title'] = "Register";
        $this->view->render('pages/register',$data);
    }

    public function run(){
        $this->model->run();
    }

}