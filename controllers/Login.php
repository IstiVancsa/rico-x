<?php
class Login extends Controller {
    function __construct(){
        parent::__construct();
        $this->index();
    }

    public function index(){
        $data['page_title'] = "Login";
        $this->view->render('pages/login',$data);
    }

    public function run(){
        $this->model->run();
    }

}