<?php
class Contact extends Controller {
    function __construct(){
        parent::__construct();
        $url = $_GET['url'];
        $url = rtrim($url,'/');
        $url = explode('/', $url);

        if(!isset($url[1])){
            $this->index();
        }
    }

    public function index(){
        $data['page_title'] = "CONTACT";
        $data['phone_number'] = "0740331231";
        $this->view->render('pages/contact',$data);
    }

}