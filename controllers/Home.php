<?php
class Home extends Controller {
    function __construct(){
        parent::__construct();

        if(isset($_GET['url'])){
            $url = $_GET['url'];
            $url = rtrim($url,'/');
            $url = explode('/', $url);

            if(!isset($url[1])){
                $this->index();
            }
        }else{
            $this->index();
        }

    }

    public function index(){
        $url = 'http://rico-x.lh/';
        if(isset($_GET['page'])){
            $page = (int)$_GET['page'];
            if($url == "http://rico-x.lh/"){
                $url .= '?page='.$page;
            }

        }else{
            $page = 1;
        }

        if(isset($_GET['search'])){
            $search = $_GET['search'];
            if($url == "http://rico-x.lh/"){
                $url .= '?search='.$search;
            }
            else{
                $url .= '&search='.$search;
            }

        }else{
            $search = '';
        }

        if(isset($_GET['sort'])){
            $sort = $_GET['sort'];
            if($url == "http://rico-x.lh/") {
                $url .= '?sort=' . $sort;
            }
            else{
                $url .= '&sort=' . $sort;
            }
        }else{
            $sort = '';
        }

        if(isset($_GET['category'])){
            $category = $_GET['category'];
            if($url == "http://rico-x.lh/") {
                $url .= '?category=' . $category;
            }
            else{
                $url .= '&category=' . $category;
            }
        }else{
            $category = '';
        }

        $data["url"] = $url;
        $path = 'models/productModel.php';
        if(file_exists($path)){
            require $path;
            $modelName = 'productModel';
            $this->model = new $modelName;
        }
        $model_data = $this->model->get_products($page,$search,$sort,$category);

        $path = 'models/categoryModel.php';
        if(file_exists($path)){
            require $path;
            $modelName = 'categoryModel';
            $categories = new $modelName;
        }
        $data["categories"] = $categories->getCategories();
        $data['products'] = $model_data["products"];
        $data['page_title'] = "Home";
        $data['currentpage'] = $page;
        $data['totalpages'] = $model_data["totalpages"];
        $this->view->render('pages/index',$data);
    }

}