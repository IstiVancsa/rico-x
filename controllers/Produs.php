<?php
class Produs extends Controller {
    function __construct(){
        parent::__construct();
        $url = $_GET['url'];
        $url = rtrim($url,'/');
        $url = explode('/', $url);

        if(!isset($url[2])){
            if($url[1] != "detalii"){
                $this->index($url[1]);
            }

        }
    }

    public function index($id){
        $path = 'models/productModel.php';
        if(file_exists($path)){
            require $path;
            $modelName = 'productModel';
            $this->model = new $modelName;
        }
        $path = 'libs/API.php';
        if(file_exists($path)){
            require $path;
            $apiName = 'API';
            $api = new $apiName;
        }
        $path = 'models/categoryModel.php';
        if(file_exists($path)){
            require $path;
            $modelName = 'categoryModel';
            $categories = new $modelName;
        }
        $data["categories"] = $categories->getCategories();
        $data['apidata'] = $api->callApi($id);
        $data['product'] = $this->model->run($id);
        $data['page_title'] = "Produs";

        $this->view->render('pages/produs',$data);
    }

    public function detalii($id){
        $path = 'models/productModel.php';
        if(file_exists($path)){
            require $path;
            $modelName = 'productModel';
            $this->model = new $modelName;
        }
        $path = 'models/categoryModel.php';
        if(file_exists($path)){
            require $path;
            $modelName = 'categoryModel';
            $categories = new $modelName;
        }
        $data["categories"] = $categories->getCategories();
        $data['product'] = $this->model->run($id);
        $data['page_title'] = "Detalii";
        $this->view->render('pages/detalii',$data);
    }
}